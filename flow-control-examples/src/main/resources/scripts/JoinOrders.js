function JoinOrders(context,messages) {
    var allOrders = [];
    messages.forEach(function (item, index) {
      var orders = JSON.parse(item.payload());
      allOrders = allOrders.concat(orders);
    });
    return JSON.stringify(allOrders, null, 4);
}